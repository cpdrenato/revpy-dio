# -*- coding: utf-8 -*-
import time
from threading import Thread, ThreadError
from typing import Optional, Callable, Any, Iterable, Mapping

BIT_1 = 0b00000001
BIT_2 = 0b00000010
BIT_3 = 0b00000100
BIT_4 = 0b00001000
BIT_5 = 0b00010000
BIT_6 = 0b00100000
BIT_7 = 0b01000000
BIT_8 = 0b10000000


class InputMonitor:
    def __init__(self, sleep_interval: float = .10, left=True):
        if not left:
            self.offset = 11
        else:
            self.offset = 0
        self.running = True
        self.sleep_interval = sleep_interval or .10

    def run(self) -> None:
        self.running = True
        with open("/dev/piControl0", "wb+", 0) as f:
            while self.run:
                f.seek(self.offset)
                inputs = f.read(2)  # 2 bytes are read
                one_through_eight = inputs[0]
                nine_through_sixteen = inputs[1]
                self.check_bits(one_through_eight)
                self.check_bits(nine_through_sixteen, False)
                time.sleep(self.sleep_interval)
        raise ThreadError('The thread has been terminated.')

    def check_bits(self, input_byte, one_through_eight=True):
        """
        Will examine one of the bytes read from the DIO and call the handle
        input function when one of the inputs is hot.
        """
        bit_range = range(1, 9) if one_through_eight else range(9, 17)
        if self.check_bit(BIT_1, input_byte):
            self.handle_input(bit_range[0])
        if self.check_bit(BIT_2, input_byte):
            self.handle_input(bit_range[1])
        if self.check_bit(BIT_3, input_byte):
            self.handle_input(bit_range[2])
        if self.check_bit(BIT_4, input_byte):
            self.handle_input(bit_range[3])
        if self.check_bit(BIT_5, input_byte):
            self.handle_input(bit_range[4])
        if self.check_bit(BIT_6, input_byte):
            self.handle_input(bit_range[5])
        if self.check_bit(BIT_7, input_byte):
            self.handle_input(bit_range[6])
        if self.check_bit(BIT_8, input_byte):
            self.handle_input(bit_range[7])

    def stop(self):
        """
        Stops the thread.
        """
        self.running = False

    def check_bit(self, bit_to_check, value):
        return value & bit_to_check == bit_to_check

    def handle_input(self, input_number: int):
        """
        When an input is activated, this function will be called along
        with the input number.  Override this to provide your functionality.
        """
        print('Input %s has been activated.' % input_number)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Start the input monitor.')
    parser.add_argument('-r', '--right', required=False, action='store_true',
                        help='If the DIO is configured to the right of '
                                   'the revpi, set this to true to change the '
                                   'offset.'
                        )
    args = parser.parse_args()
    print('RevPi is configured with left set as %s' % str(not args.right))
    input = InputMonitor(sleep_interval=.15, left=not args.right)
    input.run()
